# matlab-matrix
Perform row operations via MATLAB with pretty-print for fractions.

## GOTCHAs
Currently does not support
* unary operations such at 5R1 -> R1 
    - Workaround: 5R1 + 0R1 -> R1
* fractional operations such as 1/2R1 + 1/2R2 -> R1
    - Workaround: 0.5R1 + 0.5R2 -> R1

## Example Usage
```
T1 = [
    -1/8 0 1 3/8 -1/4 0 655 % note new last column
    15/16 1 0 -5/16 3/8 0 -465/2
    3 0 0 0 -1 1 1120    
    5/64 0 0 9/64 1/32 0 3845/8
]

T1 = row_operation(T1, '-3R2', '0R2', 'R2')         % - 3R2         -> R2
T1 = row_operation(T1, '5R2', 'R1', 'R1')           %   5R2   + R1  -> R1
T1 = row_operation(T1, '0.5R2', 'R4', 'R4')         % 1/2R2   + R4  -> R4
```

## Sample Output

```
T1 =

      -1/8            0              1              3/8           -1/4            0            655       
      15/16           1              0             -5/16           3/8            0           -465/2     
       3              0              0              0             -1              1           1120       
       5/64           0              0              9/64           1/32           0           3845/8     

-3R2 -> R2

T1 =

      -1/8            0              1              3/8           -1/4            0            655       
     -45/16          -3              0             15/16          -9/8            0           1395/2     
       3              0              0              0             -1              1           1120       
       5/64           0              0              9/64           1/32           0           3845/8     

5R2 + R1 -> R1

T1 =

    -227/16         -15              1             81/16         -47/8            0           8285/2     
     -45/16          -3              0             15/16          -9/8            0           1395/2     
       3              0              0              0             -1              1           1120       
       5/64           0              0              9/64           1/32           0           3845/8     

1/2R2 + R4 -> R4

T1 =

    -227/16         -15              1             81/16         -47/8            0           8285/2     
     -45/16          -3              0             15/16          -9/8            0           1395/2     
       3              0              0              0             -1              1           1120       
     -85/64          -3/2            0             39/64         -17/32           0           6635/8    
```