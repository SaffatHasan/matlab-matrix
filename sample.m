T1 = [
    -1/8 0 1 3/8 -1/4 0 655 % note new last column
    15/16 1 0 -5/16 3/8 0 -465/2
    3 0 0 0 -1 1 1120    
    5/64 0 0 9/64 1/32 0 3845/8
]

T1 = row_operation(T1, '-3R2', '0R2', 'R2')         % - 3R2         -> R2
T1 = row_operation(T1, '5R2', 'R1', 'R1')           %   5R2   + R1  -> R1
T1 = row_operation(T1, '0.5R2', 'R4', 'R4')         % 1/2R2   + R4  -> R4
