function result = row_operation(matrix, row_a_operation, row_b_operation, target_row)
    [row_a_multiplier, row_a_index] = split_row_operation(row_a_operation);
    [row_b_multiplier, row_b_index] = split_row_operation(row_b_operation);
    [discard, target_row_index] = split_row_operation(target_row);
    
    row_a = get_row(matrix, row_a_index);
    row_b = get_row(matrix, row_b_index);
    
    result_row = row_a * row_a_multiplier + row_b * row_b_multiplier;
    print_op(row_a_multiplier, row_a_index, row_b_multiplier, row_b_index, target_row);

    result = set_row(matrix, target_row_index, result_row);
end

function [multiplier, index] = split_row_operation(row_operation_string)
    split_str = split(row_operation_string, 'R');
    multiplier = str2double(cell2mat(split_str(1)));
    index = str2double(cell2mat(split_str(2)));
    if ismissing(multiplier)
        multiplier = 1;
    end
end

function row = get_row(matrix, index)
    row = matrix(index,:);
end

function matrix = set_row(matrix, index, new_row)
    matrix(index,:) = new_row;
end

function print_op(row_a_multiplier, row_a_index, row_b_multiplier, row_b_index, target_row)
    if (row_b_multiplier == 0)
        fprintf('%s -> %s\n', print_single_operand(row_a_index, row_a_multiplier), target_row);
        return
    end
    if (row_a_multiplier == 0)
        fprintf('%s -> %s\n', print_single_operand(row_a_index, row_a_multiplier), target_row);
        return
    end
    fprintf('%s + %s -> %s\n', print_single_operand(row_a_index, row_a_multiplier), print_single_operand(row_b_index, row_b_multiplier), target_row);
end

function [str] = print_single_operand(row_index, row_multiplier)
    if (row_multiplier == 1)
        str = sprintf("R%d", row_index);
        return;
    end
    str = sprintf("%sR%d", strtrim(rats(row_multiplier)), row_index);
end